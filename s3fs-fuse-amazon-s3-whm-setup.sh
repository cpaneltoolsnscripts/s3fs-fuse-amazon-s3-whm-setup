#!/usr/bin/env bash
## Author: Michael Ramsey
## https://gitlab.com/cpaneltoolsnscripts/s3fs-fuse-amazon-s3-whm-setup.sh
## Setup Amazon S3 remote bucket as a local mount so you can do incremental backups or backups on a server without enough diskspace to generate a backup and upload.
## How to use.
#
# If amazon s3 destination already setup as remote destination pass the SECRET_ACCESS_KEY only
# wget https://gitlab.com/cpaneltoolsnscripts/s3fs-fuse-amazon-s3-whm-setup/-/raw/master/s3fs-fuse-amazon-s3-whm-setup.sh
# bash s3fs-fuse-amazon-s3-whm-setup.sh SECRET_ACCESS_KEY
# 
# bash <(curl -sk https://gitlab.com/cpaneltoolsnscripts/s3fs-fuse-amazon-s3-whm-setup/-/raw/master/s3fs-fuse-amazon-s3-whm-setup.sh || wget --no-check-certificate -qO - https://gitlab.com/cpaneltoolsnscripts/s3fs-fuse-amazon-s3-whm-setup/-/raw/master/s3fs-fuse-amazon-s3-whm-setup.sh) SECRET_ACCESS_KEY
#
# If amazon s3 destination not already setup as remote destination pass the SECRET_ACCESS_KEY and then the ACCESS_ID
# bash s3fs-fuse-amazon-s3-whm-setup.sh SECRET_ACCESS_KEY ACCESS_ID bucket_name
##
# bash <(curl -sk https://gitlab.com/cpaneltoolsnscripts/s3fs-fuse-amazon-s3-whm-setup/-/raw/master/s3fs-fuse-amazon-s3-whm-setup.sh || wget --no-check-certificate -qO - https://gitlab.com/cpaneltoolsnscripts/s3fs-fuse-amazon-s3-whm-setup/-/raw/master/s3fs-fuse-amazon-s3-whm-setup.sh) SECRET_ACCESS_KEY ACCESS_ID bucket_name
#
# nano s3fs-fuse-amazon-s3-whm-setup.sh
#
#
# If amazon s3 destination already setup as remote destination pass the SECRET_ACCESS_KEY only
# bash s3fs-fuse-amazon-s3-whm-setup.sh SECRET_ACCESS_KEY
#
# If amazon s3 destination not already setup as remote destination pass the SECRET_ACCESS_KEY and then the ACCESS_ID
# bash s3fs-fuse-amazon-s3-whm-setup.sh SECRET_ACCESS_KEY ACCESS_ID
# 
# 
###Config#######
ACCESS_ID=$2; 
SECRET_ACCESS_KEY=$1;
backup_mount="/mnt/backup";
bucket_name=$3;
####Stop Config####

# Install epel-release repo needed for s3fs-fuse binary
yum install -y epel-release
yum install -y s3fs-fuse

# Get Amazon S3 Access_ID from backup destination.
access_id=$(grep 'aws_access_key_id:' /var/cpanel/backups/*.backup_destination| sed 's|aws_access_key_id: ||')

# If access ID is not provided as second argument use the grep value
ACCESS_ID="${2:-$access_id}"


# Get Amazon S3 bucket id and destination id from backup destination
bucket_grep=$(grep 'bucket:' /var/cpanel/backups/*.backup_destination| sed 's|bucket: ||')

# If bucket ID is not provided as third argument use the grep value
bucket="${bucket_name:-$bucket_grep}"

# Setup ~/.passwd-s3fs and /etc/passwd-s3fs
echo "${ACCESS_ID}:${SECRET_ACCESS_KEY}" > ~/.passwd-s3fs
chmod 600 ~/.passwd-s3fs
cp ~/.passwd-s3fs /etc/passwd-s3fs

# Create Mount
mkdir -p ${backup_mount}

# If existing backup destination exists get the id for disabling the remote backup transporter
destination_id=$(grep 'id:' /var/cpanel/backups/*.backup_destination|grep -v aws_access_key_id| sed 's|id: ||')

# Save bucket name to ~/.bucket-s3fs
echo $bucket > ~/.bucket-s3fs

echo 'checking current mounts';
df -h
echo "";

# Mount bucket
s3fs ${bucket} ${backup_mount} -o passwd_file=${HOME}/.passwd-s3fs

echo 'Checking mounts for new mount';
df -h| grep ${backup_mount}
echo "";

# If above mount test successful backup fstab and then add an entry
if [[ $(df -h| grep ${backup_mount}) ]] ; then
    # Setup fstab for permanent mount
    echo "Backing up /etc/fstab";
    cp /etc/fstab /etc/fstab-bak && echo "Adding to /etc/fstab"; echo ""; echo "${bucket}   ${backup_mount}    fuse.s3fs _netdev,rw,nosuid,nodev,allow_other,nonempty 0 0" | tee -a /etc/fstab
fi

echo
echo "Getting current backup settings"
whmapi1 backup_config_get
echo "========================="
echo "Setting up backupdir to new mount";
# Set WHM Backup directory default to the backup_mount
whmapi1 backup_config_set backupdir=${backup_mount}
whmapi1 backup_config_set remote_restore_staging_dir=${backup_mount}
echo "========================="
echo "Setting up backup settings to keeplocal to ensure current amazon s3 backups are not purged as their considered local now";
# setup to Retain Backups in the Default Backup Directory as the remote is now considered a local path. Enables backup mount to ensure mount from fstab is enabled. and system backup files are enabled for disaster recovery
whmapi1 backup_config_set keeplocal=1 backupfiles=1 disable_metadata=0
echo "========================="
echo "Disable the Amazon S3 upload via WHM as its already done via the local backup via new amazon s3 mount";
# Disable the remote Additional destination as its being done remotely over local mount path
whmapi1 backup_destination_set id=${destination_id} disabled=1
echo "========================="
echo "Checking mount contents unmounts and remounts automatically"
ls -lah ${backup_mount} && umount ${backup_mount} && df -h && mount -a && df -h ${backup_mount}
echo "Rebuilding backup metadata in the background"
/scripts/backups_create_metadata --schedule_rebuild


